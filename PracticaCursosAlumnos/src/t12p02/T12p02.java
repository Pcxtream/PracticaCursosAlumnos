package t12p02;

import modelo.ConexionBD;
import modelo.Curso;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

public class T12p02
{
    public static void main(String[] args)
    {
        Scanner sc=new Scanner(System.in);
        int op;
        ConexionBD bd=new ConexionBD();
        
        /* ABRIR CONEXIÓN BD **************************************************/
        try
        {
            System.out.println("Abriendo conexión BD...");
            bd.abrirConexion();
            System.out.println("Conexión abierta correctamente.");
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Error!!\n"+e.getMessage());
        }
        /**********************************************************************/
        
        do
        {
            System.out.println("");
            System.out.println("Seleccionar una opción:");
            System.out.println("-----------------------");
            System.out.println();
            System.out.println("1.- Alta de Curso.");
            System.out.println("2.- Baja de Curso.");
            System.out.println("3.- Listado de Cursos.");
            System.out.println("0.- Salir.");
            System.out.println();
            System.out.print("Opción? ");
            op=sc.nextInt();
            System.out.println("");

            switch (op)
            {
            case 1: // Alta de Curso
            {
                Curso c=new Curso();
                System.out.print("Introduce el id del curso: ");
                c.setId(sc.nextInt());
                System.out.print("Introduce el título del curso: ");
                c.setTitulo(sc.next());
                System.out.print("Introduce las horas del curso: ");
                c.setHoras(sc.nextDouble());
                System.out.println("");
                try {
                    c.altaCurso(bd);
                    System.out.println("Alta de curso correcta. ID: "+c.getId()+".");
                } catch (Exception e) {
                    System.out.println("Error!!\n"+e.getMessage());
                }
                break;
            }
            case 2: // Baja de Curso
            {
                Curso c=new Curso();
                System.out.print("Introduce el id del curso: ");
                c.setId(sc.nextInt());
                System.out.println("");
                try {
                    c.bajaCurso(bd);
                    System.out.println("Baja de curso correcta. ID: "+c.getId()+".");
                } catch (Exception e) {
                    System.out.println("Error!!\n"+e.getMessage());
                }
                break;
            }
            case 3: // Listado de Cursos
            {
                System.out.println("LISTADO DE CURSOS");
                System.out.println("-----------------");
                List<Curso> tCursos=new ArrayList<>();
                try {
                    Curso.listadoCursos(bd,tCursos);
                    Collections.sort(tCursos);
                    for (Curso c: tCursos)
                        System.out.printf("ID: %-6d TÍTULO: %-10s HORAS: %6.2f\n",
                                            c.getId(),c.getTitulo(),c.getHoras());
                } catch (Exception e) {
                    System.out.println("Error!!\n"+e.getMessage());
                }                
                break;
            }   
            case 0: // Salir
            {
                /* CERRAR CONEXIÓN BD *****************************************/
                try
                {
                    System.out.println("Cerrando conexión BD...");
                    bd.cerrarConexion();
                    System.out.println("Conexión cerrada correctamente.");
                } catch (Exception e) {
                    System.out.println("Error!!\n"+e.getMessage());
                }
                /**************************************************************/
                break;
            }
            default:
            {
                System.out.println("Opción incorrecta!!");
                System.out.println("");
            }
            }
        }
        while (op!=0);
        System.out.println("");
    
    }
    
}
